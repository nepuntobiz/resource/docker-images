
user nobody;
pid /run/nginx.pid;
worker_processes 2;
worker_rlimit_nofile 65535;

events {
	multi_accept on;
	worker_connections 65535;
}

http {
	charset utf-8;
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	server_tokens off;
	log_not_found off;
	types_hash_max_size 2048;
	client_max_body_size 500M;

	# MIME
	include mime.types;
	default_type application/octet-stream;

	# logging
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log warn;

	server {
		listen 80;
		listen [::]:80;

		server_name _;
		set $base /var/www/;
		root $base/public;

		# index.php
		index index.php;

		# index.php fallback
		location / {
			try_files $uri $uri/ /index.php?$query_string;
		}

		# handle .php
		location ~ \.php$ {
			# fastcgi
			fastcgi_pass				unix:/var/run/sockets/php.sock;
			fastcgi_index				index.php;
			fastcgi_split_path_info		^(.+\.php)(/.+)$;
			include fastcgi_params;

			fastcgi_param				SCRIPT_FILENAME $document_root$fastcgi_script_name;
			fastcgi_param               SCRIPT_NAME $fastcgi_script_name;
			fastcgi_param				PHP_ADMIN_VALUE open_basedir=$base/:/var/run/secrets:/run/secrets:/usr/lib/php/:/tmp/;
			fastcgi_intercept_errors	off;

			fastcgi_buffer_size				128k;
			fastcgi_buffers					256 16k;
			fastcgi_busy_buffers_size		256k;
			fastcgi_temp_file_write_size	256k;
		}

		# security headers
		add_header X-Frame-Options "SAMEORIGIN" always;
		add_header X-XSS-Protection "1; mode=block" always;
		add_header X-Content-Type-Options "nosniff" always;
		add_header Referrer-Policy "no-referrer" always;
		add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
		
		# . files
		location ~ /\. {
			deny all;
		}

		# gzip
		gzip on;
		gzip_vary on;
		gzip_proxied any;
		gzip_comp_level 6;
		gzip_types text/plain text/css text/xml application/json application/javascript application/xml+rss application/atom+xml image/svg+xml;
	}
}

